class AuthService

  attr_accessor :auth_params, :device

  def initialize(auth_params)
    self.auth_params = auth_params
  end

  def renew_tokens
    self.device = Device.find_by(
        auth_token: auth_params[:auth_token],
        refresh_token: auth_params[:refresh_token]
    )
    return unless valid_refresh_token
    device.generate_auth_tokens
    { auth_token: device.auth_token, refresh_token: device.refresh_token }
  end

  def ensure_token
    self.device = Device.find_by_token( auth_params[:guid])
    return unless device
    check_for_device_update
    device.generate_auth_tokens
    { auth_token: device.auth_token, refresh_token: device.refresh_token }
  end

  def check_for_device_update
    return unless valid_device_params
    device.update_data ({ push_token: auth_params[:push_token], os: auth_params[:os] })
  end

  def valid_refresh_token
    return unless device
    device.try(:refresh_token_expiry) > Time.now
  end

  def valid_device_params
    return false unless device
    device.os != auth_params[:os] ||
        device.push_token != auth_params[:push_token]
  end
end