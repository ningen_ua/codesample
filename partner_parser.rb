class PartnersParser
  prepend SimpleCommand

  SOURCE_LINK = 'http://ocs-prod-writeapi.ocs.ostmodern.co.uk/api/partners/'.freeze

  def initialize
    @partners_data_for_insert = []
    @partners_data_for_update = []
  end

  def call
    pull_partners_data
    insert_partners_data
    update_partners_data
  end

  def ipd
    insert_partners_data
  end
  private

  attr_reader :partners_data_for_insert, :partners_data_for_update

  def pull_partners_data
    raw_data = RestClient.get SOURCE_LINK
    partners_data = JSON.parse(raw_data).dig('objects') || []
    existed_ids = Partner.where(uid: partners_data.map { |pd| pd['uid'] }).pluck(:uid)
    partners_data.each do |pd|
      (existed_ids.include?(pd['uid']) ? partners_data_for_update : partners_data_for_insert) << pd
    end
  end

  def insert_partners_data
    partners = []
    partners_data_for_insert.each do |pd|
      partners << Partner.new(
          name:             pd['name'],
          from_skylark:     true,
          synced:           true,
          created:          pd['created'],
          partner_url:      pd['partner_url'],
          uid:              pd['uid'],
          original_data:    pd,
          password:         SecureRandom.hex(10)
      )
    end
    Partner.import partners
  end

  def update_partners_data
    partners_data_for_update.each do |pd|
      partner = Partner.find_by(uid: pd['uid'])
      next unless partner
      partner.update(
          name:             pd['name'],
          created:          pd['created'],
          partner_url:      pd['partner_url'],
          uid:              pd['uid'],
          original_data:    pd
      )
    end
  end
end