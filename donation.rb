
# == Schema Information
#
# Table name: donations
#
#  id                    :integer          not null, primary key
#  amount                :float
#  user_id               :integer
#  user_name             :string
#  user_email            :string
#  project_id            :integer
#  project_name          :string
#  designation           :string
#  recurring_donation_id :integer
#  payment_card_id       :integer
#  payment_card_last4    :string
#  payment_card_type     :string
#  processor_charge_id   :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  token                 :string
#  designation_id        :integer
#  anonymous             :boolean
#  donation_type         :string
#  application_fee       :float
#  processing_type       :integer          default(0)
#  details               :text
#  processor_fees        :text
#  status                :integer          default(0)
#  amount_recipient      :float
#  amount_application    :float
#

class Donation < ActiveRecord::Base

  include Tokenable

  serialize :details, Hash
  serialize :processor_fees, Hash

  attr_accessor :manually

  PROCESSOR_FEE_PERCENT = 2.9
  PROCESSOR_FEE_FIXED = 30 #cents
  PROCESSOR_FEE_PER_TRANSFER = 25#cents

  belongs_to  :user
  belongs_to  :project , touch: true
  belongs_to  :recurring_donation
  belongs_to  :payment_card

  before_create   :add_donation_info
  validates_presence_of :payment_card_id, unless: :manually
  validates_presence_of :user_id,  :project_id, :amount
  validates_uniqueness_of :processor_charge_id, allow_blank: true

  default_scope { order('id asc') }
  STATUS_PENDING = 'pending'
  STATUS_PAID = 'paid'
  STATUS_FAILED = 'failed'

  enum status: [STATUS_PENDING,STATUS_PAID,STATUS_FAILED]
  enum processing_type: [RecipientAccount::STRIPE_CONNECT, RecipientAccount::BANK_ACCOUNT]

  def make!(metadata = {}, description = nil)
    if self.valid?
      amount_cents = (self.amount * 100).to_i
      metadata.merge! self.metadata_key => self.token

      description = "donation for #{project.name}" if description.nil?

      donation_params = {
          amount: amount_cents,
          currency: 'usd',
          description: description,
          metadata: metadata
      }

      fees = calc_fees
      self.application_fee = project.recipient_account.application_fee
      self.amount_application = (fees.amount_application / 100).round(2)

      self.processing_type = project.recipient_account.processing_type

      if project.recipient_account.stripe_connect?
        token = Stripe::Token.create(
            {
                customer: user.processor_customer_id,
                card: payment_card.processor_card_id
            },
            project.recipient_account.processor_token
        )

        donation_params.merge!(
            source: token.id,
            application_fee: fees.amount_application.to_i
        )
        charge = Stripe::Charge.create(
            donation_params,
            project.recipient_account.processor_token
        )

        if fees.amount_application.to_i > 0
          Stripe::Transfer.create(
              amount: fees.amount_application.to_i,
              currency: 'usd',
              recipient: 'self',
              source_transaction: charge.id
          )
        end

        self.amount_recipient = (fees.amount_recipient / 100).round(2)

      else

        donation_params.merge!(
            source: payment_card.processor_card_id,
            customer: user.processor_customer_id
        )
        charge = Stripe::Charge.create(donation_params)
        amount_recipient_cents = fees.amount_recipient - PROCESSOR_FEE_PER_TRANSFER
        self.amount_recipient = (amount_recipient_cents / 100).round(2)

        #should be created two transfers
        Stripe::Transfer.create(
            amount: amount_recipient_cents.to_i,
            currency: 'usd',
            recipient: project.recipient_account.bank_account_id,
            source_transaction: charge.id
        )

        if fees.amount_application.to_i > 0
          Stripe::Transfer.create(
              amount: fees.amount_application.to_i,
              currency: 'usd',
              recipient: 'self',
              source_transaction: charge.id
          )
        end

      end

      # balance_transaction = Stripe::BalanceTransaction.retrieve charge.balance_transaction
      # fee_details = Hash[balance_transaction.fee_details.collect{|b| [b['type'], b['amount']]}]
      # self.processor_fees = fee_details
      # unless project.recipient_account.stripe_connect?
      #   self.processor_fees[:transfer_fee] = PROCESSOR_FEE_PER_TRANSFER
      # end

      self.processor_charge_id = charge.id
      self.save!
      #send receipt to donor
      UserMailer.donor_email(user: self.user, donation: self).deliver_later

      #send email to recipient

      User.admins.each do |admin|
        UserMailer.recipient_email(user: admin, donation: self).deliver_later
      end

      #self.project.admins.each do |admin|
      #  UserMailer.recipient_email(user: admin, donation: self).deliver_later
      #end
    else
      # ap errors
    end
  rescue Exception => e
    errors.add(:base, e.message)
    false
  end

  def add_donation_info
    if self.anonymous?
      self.user_email = 'Hidden'
      self.user_name = 'Anonymous'
    else
      self.user_email = user.email
      self.user_name = user.full_name
    end

    self.project_name = project.name
    if payment_card.present?
      self.payment_card_last4 = payment_card.card_last4
      self.payment_card_type = payment_card.card_type
    end
  end


  def recurring?
    donation_type != Give::ONETIME
  end


  private

  def calc_fees
    amount_cents = (self.amount * 100).to_i
    app_fee = self.project.recipient_account.application_fee

    fee_processor = ((amount_cents / 100 * PROCESSOR_FEE_PERCENT) + PROCESSOR_FEE_FIXED).round.to_f
    fee_total = ((amount_cents / 100 * (app_fee + PROCESSOR_FEE_PERCENT)) + PROCESSOR_FEE_FIXED).round.to_f

    fee_application = fee_total - fee_processor
    amount_recipient = amount_cents - fee_total

    OpenStruct.new(
        amount_cents: amount_cents,
        amount_recipient: amount_recipient,
        amount_processor: fee_processor,
        amount_application: fee_application,
        amount_total: fee_total
    )

  end


end
